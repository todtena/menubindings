package com.todtenkopf.menubinding;

import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;

import com.todtenkopf.mvvm.MenuCommandBindings;
import com.todtenkopf.mvvm.ViewModelActivity;
import com.todtenkopf.mvvm.ViewModelBase;

import static com.todtenkopf.mvvm.MenuCommandBindings.*;

public class MainActivity extends ViewModelActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Nullable
    @Override
    protected ViewModelBase createViewModel() {
        MainViewModel vm = new MainViewModel();
        addMenuBinding(R.id.edit_mode, vm.editCommand, EnableBinding.Visible);
        addMenuBinding(R.id.save_layout, vm.saveLayoutCommand, EnableBinding.Visible);
        return vm;
    }
}
