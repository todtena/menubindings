package com.todtenkopf.menubinding;

import com.todtenkopf.mvvm.ViewModelBase;

public class MainViewModel extends ViewModelBase {
    private boolean mInEditMode;

    public MainViewModel() {
        refreshCommands();
    }

    public CommandVM editCommand = new CommandVM() {

        @Override
        public void refresh() {
            isEnabled(!mInEditMode);
        }

        @Override
        public void execute() {
            mInEditMode = true;
            refreshCommands();
        }
    };

    public CommandVM saveLayoutCommand = new CommandVM() {
        @Override
        public void refresh() {
            isEnabled(mInEditMode);
        }

        @Override
        public void execute() {
            mInEditMode = false;
            refreshCommands();
            // call out to injected context to execute a service call
        }
    };
}
