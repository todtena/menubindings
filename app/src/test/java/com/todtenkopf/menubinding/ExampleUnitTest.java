package com.todtenkopf.menubinding;

import com.todtenkopf.mvvm.MenuCommandBindings;
import com.todtenkopf.mvvm.ViewModelBase;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void editEnablesSaveAndHidesEdit() {
        MainViewModel vm = new MainViewModel();
        assertFalse("save enabled before edit", vm.saveLayoutCommand.isEnabled());
        assertTrue("edit enabled before edit", vm.editCommand.isEnabled());
        vm.editCommand.execute();
        assertTrue("save enabled after edit", vm.saveLayoutCommand.isEnabled());
        assertFalse("edit enabled after edit", vm.editCommand.isEnabled());
    }

    @Test
    public void saveEnablesEditAndHidesSave() {
        MainViewModel vm = new MainViewModel();
        vm.editCommand.execute();
        assertTrue("save enabled before save", vm.saveLayoutCommand.isEnabled());
        assertFalse("edit enabled before save", vm.editCommand.isEnabled());
        vm.saveLayoutCommand.execute();
        assertFalse("save enabled after save", vm.saveLayoutCommand.isEnabled());
        assertTrue("edit enabled after save", vm.editCommand.isEnabled());
    }
}